class User < ApplicationRecord
	acts_as_paranoid
	devise :database_authenticatable, :registerable,
	     :recoverable, :rememberable, :trackable, :validatable

	has_many :posts
end
