class Post < ApplicationRecord
	acts_as_paranoid
	has_many :comments, dependent: :destroy
	belongs_to :user

	validates :title, presence: true, length: { minimum: 5 }
	validates :body, presence: true

	accepts_nested_attributes_for :comments

	def user
		User.with_deleted.find(user_id)
	end

	has_attached_file :image, styles: { large: "600x600>", 
										medium: "300x300>", 
										small: "150x150#", 
										thumb: "100x100#" }, 
										default_url: "/images/:style/missing.png"
  	validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
